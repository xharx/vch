﻿--修改日期
--2018.12.18
--所有涉及到登录、验证等信息都放到AcctCtl数据库里
--这样当软件新建帐套时，就不需要反复执行脚本了
/*
   用来记录帐套用户的Token

*/
use AcctCtl
CREATE TABLE [dbo].[t_Web_CurrentToken](
	[FUserID] [int] NULL,
	[FAcctKey] [int] NULL,
	[FToken] [varchar](255) NULL
) ON [PRIMARY]
GO
--用户token历史记录表
CREATE TABLE [dbo].[t_Web_HistoryToken](
	[FUserID] [int] NULL,
	[FToken] [varchar](255) NULL,
	[FCreateDate] [datetime] NULL,
	[FClientIP] [varchar](255) NULL,
	[FClientOtherInfo] [varchar](255) NULL,
	[FAcctKey] [int] NULL,
	[FInvalidDate] [datetime] NULL
) ON [PRIMARY]
GO
--API访问日志记录表
CREATE TABLE [dbo].[t_WebApi_Log](
	[FUserID] [int] NULL,
	[FAPIName] [varchar](255) NULL,
	[FParam] [varchar](max) NULL,
	[FCallTime] [datetime] NULL,
	[FClientInfo] [varchar](255) NULL,
	[FReason] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
